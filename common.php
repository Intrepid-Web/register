<?php
/**
 * Intrepid-Web.NET
 *
 * Source code is free to use for anyone, however please leave a credit
 * to Intrepid-Web.NET if and when using any part of this code is used.
 *
 * @copyright   Copyright (C) Intrepid-Web.NET. (www.intrepid-web.net)
 */

# Setup
$cfg = array();

# Config
include "./config.php";

# Language
$lang_file = './include/lang/lang_'.$cfg['language'].'.xml';
if(file_exists($lang_file)) {
    $xml = simplexml_load_file($lang_file);

    foreach($xml->translations->string as $key => $string) {
        $lang_key = trim((string)$string->attributes()->key);
        $lang_value = (string)$string;
        $lang[$lang_key] = $lang_value;
    }
}

# Get functions
include "./include/functions.php";

# Database class
if(version_compare(PHP_VERSION, '5.3.0', '<'))  {
    include './include/db/mssql.php';
} else {
    include './include/db/sqlsrv.php';
}


# Load our database class
$db = new db($cfg['database']['user'], $cfg['database']['pass'], $cfg['database']['host']);

# Select our database
$db->select_database($cfg['database']['name']);
