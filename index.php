<?php
/**
 * Intrepid-Web.NET
 * 
 * Source code is free to use for anyone, however please leave a credit 
 * to Intrepid-Web.NET if and when using any part of this code is used.
 * 
 * @copyright   Copyright (C) Intrepid-Web.NET. (www.intrepid-web.net)
 */

# Common
include "./common.php";

# Get user input
$page = filter_input(INPUT_GET | INPUT_POST, 'page', FILTER_SANITIZE_SPECIAL_CHARS);

$username = filter_input(INPUT_POST, 'register_username', FILTER_SANITIZE_SPECIAL_CHARS);
$email = filter_input(INPUT_POST, 'register_email', FILTER_SANITIZE_SPECIAL_CHARS);
$confirm_email = filter_input(INPUT_POST, 'register_confirm_email', FILTER_SANITIZE_SPECIAL_CHARS);
$password = filter_input(INPUT_POST, 'register_password', FILTER_SANITIZE_SPECIAL_CHARS);
$confirm_password = filter_input(INPUT_POST, 'register_confirm_password', FILTER_SANITIZE_SPECIAL_CHARS);

# Switch our page
switch($page) {
    /**
     * Success Page
     */
    case "success":
        end_message(_l('Success'), _l('You have successfully registered your account.'), 'success');
    break;

    /**
     * Registration Page
     */
    default:
        # Variables
        $success = true;
        $messages = array();

        # Posted
        if($_SERVER['REQUEST_METHOD'] === 'POST') {

            # Error checking
            if($username == '' || $email == '') {
                $success = false;
                $messages[] = _l("Some fields were left blank. All fields must be filled");
            }

            if(!preg_match($cfg['username']['regex'], $username))  {
                $success = false;
                $messages[] = _l("Invalid username provided. Username can only contain letters and numbers.");
            }

            if(strlen($username) < $cfg['username']['minimum_length'] || strlen($username) > $cfg['username']['maximum_length']) {
                $success = false;
                $messages[] = _l("Username must be between %d to %d characters in length.", $cfg['password']['minimum_length'], $cfg['password']['maximum_length']);
            }

            if($email != $confirm_email) {
                $success = false;
                $messages[] = _l("E-Mail confirmation (re-type) did not match. Please check for typos.");
            } elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $success = false;
                $messages[] = _l("Invalid e-mail address provided. Please make sure you enter a valid and working, email address");
            }

            if($password != $confirm_password) {
                $success = false;
                $messages[] = _l("Confirmation password does not match. Please check for typos.");
            } elseif(!preg_match($cfg['password']['regex'], $password))  {
                $success = false;
                $messages[] = _l("Invalid password provided. Password can only contain letters and numbers.");
            }

            if(strlen($password) < $cfg['password']['minimum_length'] || strlen($password) > $cfg['password']['maximum_length']) {
                $success = false;
                $messages[] = _l("Password must be between %d to %d characters in length.", $cfg['password']['minimum_length'], $cfg['password']['maximum_length']);
            }

            # No errors? work with the database
            if($success) {
                # Check if username has been taken
                $sql = 'SELECT id FROM '.$cfg['database']['user_table'].' WHERE id = CONVERT(binary, ?)';
                if(!($result = $db->query($sql, array($username)))) {
                    end_message(_l("Query Error"), _l("Failed to query the database when check your username"));
                }

                if($db->has_rows($result)) {
                    $success = false;
                    $messages[] = _l("Sorry, the username you have chosen has already been taken.");
                }
                # Check if email has been taken
                $sql = 'SELECT id FROM '.$cfg['database']['user_table'].' WHERE email = ?';
                if(!($result = $db->query($sql, array($email)))) {
                    end_message(_l("Query Error"), _l("Failed to query the database when checking your email"));
                }

                if($db->has_rows($result)) {
                    $success = false;
                    $messages[] = _l("Sorry, the e-mail address you have chosen has already been used.");
                }

                # No more errors? Let's register...
                if($success) {
                    # Insert query
                    $sql = "INSERT INTO ".$cfg['database']['user_table']." (id, password, email) VALUES (CONVERT(BINARY, ?), CONVERT(BINARY,?), ?)";
                    if(!($result = $db->query($sql, array($username, $password, $email)))) {
                        end_message(_l("Query Error"), _l("Unable to finish your registration"));
                    }

                    # Exit the page
                    header('Location: '.$_SERVER['PHP_SELF'].'?page=success');
                    exit;
                }
            }
        }

        page_head();
        ?>
            <?php if(!empty($messages)): ?>
            <div class="alert alert-block alert-error">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <h4><?php echo _l('Error Occurred!'); ?></h4>
                <ul>
                    <?php foreach($messages as $message): ?>
                    <li><?php echo $message; ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <?php endif; ?>
            <form method="POST" class="form-horizontal">
                <fieldset>
                    <legend><?php echo _l('Register'); ?></legend>
                    <div class="control-group">
                        <label class="control-label" for="username"><?php echo _l('Username'); ?></label>
                        <div class="controls">
                            <input name="register_username" type="text" id="username" placeholder="Username" value="<?php echo $username; ?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="email"><?php echo _l('Email'); ?></label>
                        <div class="controls">
                            <input name="register_email" type="email" id="email" placeholder="<?php echo _l('A valid email address'); ?>" value="<?php echo $email; ?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="confirm-email"><?php echo _l('Confirm Email'); ?></label>
                        <div class="controls">
                            <input name="register_confirm_email" type="email" id="confirm-email" placeholder="<?php echo _l('Retype your email address'); ?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="password"><?php echo _l('Password'); ?></label>
                        <div class="controls">
                            <input name="register_password" type="password" id="password" placeholder="<?php echo _l('Password'); ?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="confirm-password"><?php echo _l('Confirm Password'); ?></label>
                        <div class="controls">
                            <input name="register_confirm_password" type="password" id="confirm-password" placeholder="<?php echo _l('Retype your password'); ?>">
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary"><?php echo _l('Register'); ?></button>
                    </div>
                </fieldset>
            </form>
        <?php
        page_foot();
}