<?php
/**
 * Intrepid-Web.NET
 * 
 * Source code is free to use for anyone, however please leave a credit 
 * to Intrepid-Web.NET if and when using any part of this code is used.
 * 
 * @copyright   Copyright (C) Intrepid-Web.NET. (www.intrepid-web.net)
 */

class db {

	private $username;
	private $password;
	private $host;
	private $port;
	private $connection = false;

    /**
     * @param $username
     * @param $password
     * @param $host
     * @param int $port
     */
    public function __construct($username, $password, $host, $port = 1433) {
		# Ensure we have our module installed
		if(!(extension_loaded('sqlsrv'))) {
            end_message(_l("PHP Error"), _l("PHP Extension (module) %s could not be found.", 'SQLSRV'));
		}

        # Make sure we got some data
        if($username == '' || $password == '' || $host == '') {
            end_message(_l("Config Error"), _l("Some database configuration settings were not set"));
        }

		# Setup variables
		$this->username = $username;
		$this->password = $password;
		$this->host = $host;
		$this->port = $port;

        # Connect to the database
        $this->connect();
	}

    /**
     * Open connection
     */
    public function connect() {
		# Setup connection info
		$connection_info = array('UID' => $this->username, 'PWD' => $this->password, 'ReturnDatesAsStrings' => true);

		# Connect
		$this->connection = sqlsrv_connect($this->host, $connection_info);
		if($this->connection == false) {
			end_message(_l("DB Error"), _l("Error connecting to the database server."), 'error', $this->getError());
		}
	}

    /**
     * Select current database
     * @param $database_name
     * @return bool|resource
     */
    public function select_database($database_name) {
        if(!$this->connection) {
            return false;
        }

        return $this->query('USE '.$database_name);
    }

    /**
     * Query
     * @param $sql
     * @param array $params
     * @return bool|resource
     */
    public function query($sql, $params = array()) {
        if(!$this->connection) {
            return false;
        }

        $query = sqlsrv_query($this->connection, $sql, $params, array('Scrollable' => SQLSRV_CURSOR_CLIENT_BUFFERED));
        if($query === FALSE) {
            end_message(_l("DB Error"), _l("Error occurred while running a query."), 'error', array('sql' => $sql, 'error' => $this->getError()));
        }

        return $query;
    }

    /**
     * Fetch Array
     * @param $query
     * @param int $fetchType
     * @return array|false|null
     */
    public function fetch_array($query, $fetchType = SQLSRV_FETCH_ASSOC) {
        if(!$this->connection) {
            return false;
        }

        return sqlsrv_fetch_array($query, $fetchType);
    }

    /**
     * Number of rows
     * @param $query
     * @return bool|int
     */
    public function num_rows($query) {
        if(!$this->connection) {
            return false;
        }

        $num_rows = sqlsrv_num_rows($query);

        return $num_rows ? $num_rows : TRUE;
    }

    /**
     * Check if query has rows
     * @param $query
     * @return bool
     */
    public function has_rows($query) {
        if(!$this->connection) {
            return false;
        }
        return (boolean) sqlsrv_has_rows($query);
    }

    /**
     * Returns any DB errors
     * @return array|null
     */
    public function getError() {
		return sqlsrv_errors();
	}

    /**
     * Close connection
     * @return bool
     */
    public function close() {
        if(!$this->connection) {
            return false;
        }

        return sqlsrv_close($this->connection);
    }
}