<?php
/**
 * Intrepid-Web.NET
 *
 * Source code is free to use for anyone, however please leave a credit
 * to Intrepid-Web.NET if and when using any part of this code is used.
 *
 * @copyright   Copyright (C) Intrepid-Web.NET. (www.intrepid-web.net)
 */

class db {

    private $username;
    private $password;
    private $host;
    private $connection = false;

    /**
     * @param $username
     * @param $password
     * @param $host
     * @param int $port
     */
    public function __construct($username, $password, $host) {
        # Ensure we have our module installed
        if(!(extension_loaded('mssql'))) {
            end_message(_l("PHP Error"), _l("PHP Extension (module) %s could not be found.", 'MSSQL'));
        }

        # Make sure we got some data
        if($username == '' || $password == '' || $host == '') {
            end_message(_l("Config Error"), _l("Some database configuration settings were not set"));
        }

        # Not ready
        end_message("In development", "Sorry, this module is still in development and cannot be used yet.");

        # Setup variables
        $this->username = $username;
        $this->password = $password;
        $this->host = $host;

        # Connect to the database
        $this->connect();
    }

    /**
     * Open connection
     */
    public function connect() {
        # Setup connection info
        $connection_info = array('UID' => $this->username, 'PWD' => $this->password, 'ReturnDatesAsStrings' => true);

        # Connect
        $this->connection = mssql_connect($this->host, $this->username, $this->password);
        if($this->connection == false) {
            end_message(_l("DB Error"), _l("Error connecting to the database server."), 'error', $this->getError());
        }
    }

    /**
     * Select current database
     * @param $database_name
     * @return bool|resource
     */
    public function select_database($database_name) {
        if(!$this->connection) {
            return false;
        }

        return mssql_select_db($database_name);
    }

    /**
     * Query
     * @param $sql
     * @param array $params
     * @return bool|resource
     */
    public function query($sql, $params = array()) {
        if(!$this->connection) {
            return false;
        }

        # Check if we have params and match
        if(!empty($params)) {
            $total_params = $params;
            $tokens = preg_split('/((?<!\\\)[&?!])/', $sql, -1, PREG_SPLIT_DELIM_CAPTURE);

            # not yet finished..
            exit;
        }

        $query = mssql_query($sql, $this->connection);
        if($query === FALSE) {
            end_message(_l("DB Error"), _l("Error occurred while running a query."), 'error', array('sql' => $sql, 'error' => $this->getError()));
        }

        return $query;
    }



    /**
     * Fetch Array
     * @param $query
     * @param int $fetchType
     * @return array|false|null
     */
    public function fetch_array($query, $fetchType) {
        if(!$this->connection) {
            return false;
        }

        return mssql_fetch_assoc($query);
    }

    /**
     * Number of rows
     * @param $query
     * @return bool|int
     */
    public function num_rows($query) {
        if(!$this->connection) {
            return false;
        }

        return mssql_num_rows($query);
    }

    /**
     * Check if query has rows
     * @param $query
     * @return bool
     */
    public function has_rows($query) {
        if(!$this->connection) {
            return false;
        }
        return (mssql_num_rows($query) > 0) ? true : false;
    }

    /**
     * Returns any DB errors
     * @return array|null
     */
    public function getError() {
        return mssql_get_last_message();
    }

    /**
     * Close connection
     * @return bool
     */
    public function close() {
        if(!$this->connection) {
            return false;
        }

        return mssql_close($this->connection);
    }
}