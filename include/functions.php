<?php
/**
 * Intrepid-Web.NET
 * 
 * Source code is free to use for anyone, however please leave a credit 
 * to Intrepid-Web.NET if and when using any part of this code is used.
 * 
 * @copyright   Copyright (C) Intrepid-Web.NET. (www.intrepid-web.net)
 */

function page_head($title = 'Register') {
    global $header_included;
    $header_included = true;
?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/bootstrap-responsive.min.css" rel="stylesheet">
    <style type="text/css">
        body {
            padding-top: 30px;
        }

        .container {
            width: 40%;
        }
    </style>
</head>

<body>
    <div class="container">
<?php
}

function page_foot() {
?>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
</body>
</html>
<?php
}

function end_message($title, $message, $type = 'error', $debug = array()) {
    global $cfg;

    if(!isset($header_included)) {
        page_head($title);
    }
?>
	<div class="alert alert-block alert-<?php echo $type; ?>">
		<h4><?php echo $title; ?></h4>
		<?php echo $message; ?>
		<?php if(!empty($debug) && $cfg['enable_debug'] == true): ?>
        <br/><br/>
        <strong>DEBUG:</strong><br/>
        <pre><?php echo nl2br(var_export($debug)); ?></pre>
		<?php endif; ?>
	</div>
<?php
    page_foot();
	exit;
}


/**
 * Translation
 * @return string
 */
function _l()
{
    global $lang;

    $args = func_get_args();

    $string = trim(array_shift($args));

    if(isset($lang[$string])) {
        return (!empty($args)) ? trim(vsprintf($lang[$string], $args)) : trim($lang[$string]);
    } else {
        return (!empty($args)) ? trim(vsprintf($string, $args)) : trim($string);
    }
}