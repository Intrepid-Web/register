<?php
/**
 * Intrepid-Web.NET
 * 
 * Source code is free to use for anyone, however please leave a credit 
 * to Intrepid-Web.NET if and when using any part of this code is used.
 * 
 * @copyright   Copyright (C) Intrepid-Web.NET. (www.intrepid-web.net)
 */

/**
Before using this script, you must have the "email" column in your 
RF Account table. If you do not, please run this script below:
-------------------------------------------------------------------
ALTER TABLE dbo.tbl_rfaccount ADD email varchar(255) NULL
GO
ALTER TABLE dbo.tbl_rfaccount ADD CONSTRAINT DF_tbl_rfaccount_accounttype DEFAULT 0 FOR accounttype
GO
ALTER TABLE dbo.tbl_rfaccount ADD CONSTRAINT DF_tbl_rfaccount_birthdate DEFAULT (getdate()) FOR birthdate
GO
**/

# Edit the configuration below
$cfg['database']['name'] = '';                       // Database name (e.g. USER)
$cfg['database']['user'] = '';                       // SQL username
$cfg['database']['pass'] = '';                       // Password to your SQL user
$cfg['database']['host'] = '(local)';                // Host of your Microsoft SQL Server (e.g. 127.0.0.1)
$cfg['database']['user_table']  = 'tbl_rfaccount';   // Name of your RF Account table (e.g. tbl_RFAccount or tbl_LUAccount)

$cfg['language'] = 'en';
$cfg['enable_debug'] = true;

$cfg['username']['minimum_length'] = 4;
$cfg['username']['maximum_length'] = 12;
$cfg['username']['regex'] = '/^(^[a-zA-Z][a-zA-Z0-9]+)$/';

$cfg['password']['minimum_length'] = 4;
$cfg['password']['maximum_length'] = 12;
$cfg['password']['regex'] = '/^([a-zA-Z0-9]+)$/';
# End